# chat_client

A new flutter module project.

## Getting Started

`$flutter pub get
 $flutter run 
`
# For Anfroid lib 
`$flutter build aar`

# For iOS lib
`$flutter build ios-framework --cocoapods`


For help getting started with Flutter, view our online
[documentation](https://flutter.dev/).

For instructions integrating Flutter modules to your existing applications,
see the [add-to-app documentation](https://flutter.dev/docs/development/add-to-app).
