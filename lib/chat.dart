// Copyright 2021 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:async';
import 'dart:math';
import 'package:chat_client/link_text.dart';
import 'package:chat_client/three_bounce.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:chat_client/grpc/generated/bot.pbgrpc.dart' as bot;
// import 'package:sound_stream/sound_stream.dart';
import 'dart:io' show Platform;

import 'package:url_launcher/url_launcher.dart';


class Chat extends StatefulWidget {
  Chat({Key? key,this.isLoggedIn = true}) : super(key: key);
  bool isLoggedIn;
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  final List<ChatMessage> _messages = <ChatMessage>[];
  final TextEditingController _textController = TextEditingController();
  final List<String> query = [];
  bool _isRecording = false;
  bool _isLoading = false;
  String? mobile;

  // RecorderStream _recorder = RecorderStream();
  // StreamSubscription? _recorderStatus;
  // StreamSubscription<List<int>>? _audioStreamSubscription;
  // BehaviorSubject<List<int>>? _audioStream;

  final channel = ClientChannel(
    '52.66.207.18',
    port: 5000,
    options: const ChannelOptions(
      credentials: ChannelCredentials.insecure(),
    ),
  );
  late bot.PostMessageServiceClient _client;

  @override
  void initState() {
    super.initState();

    _client = bot.PostMessageServiceClient(channel);
    initPlugin();
    if(widget.isLoggedIn){
      final mobiles = ['7676704243','9102831623'];
      mobile = '9102831623';//mobiles[Random().nextInt(mobiles.length)];
    }
    handleSubmitted('hi', isAdd: false);
  }



  @override
  void dispose() {
    // _recorderStatus?.cancel();
    // _audioStreamSubscription?.cancel();
    channel.shutdown();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlugin() async {
    // _recorderStatus = _recorder.status.listen((status) {
    //   if (mounted) {
    //     setState(() {
    //       _isRecording = status == SoundStreamStatus.Playing;
    //     });
    //   }
    // });
    // await Future.wait([
    //  _recorder.initialize()
    // ]);



  }


  void stopStream() async {
    // await _recorder.stop();
    // await _audioStreamSubscription?.cancel();
    // await _audioStream?.close();
  }

  void handleSubmitted(text,{bool isAdd=true}) async {
    print(text);
    _textController.clear();

    //TODO Dialogflow Code
    ChatMessage message = ChatMessage(
      text: text,
      name: "You",
      type: true,
    );

    if(isAdd){
      setState(() {
        _messages.insert(0, message);
        _isLoading = true;
      });
    }

    String? fulfillmentText = "";
    try {
      final request = bot.UserQuery();
      request.message = text;
      request.prevQueries.addAll(query);
      if(mobile != null){
        request.mobile = mobile!;
      }
      bot.Response response = await _client.postMessages(request);
      fulfillmentText = response.message;
      print('***************** $fulfillmentText, ${response.fulfillmentText}');
      if(fulfillmentText == null || fulfillmentText.isEmpty){
        fulfillmentText = response.fulfillmentText;
      }
    }
    catch(e) {
      print(e);
    }

    if(fulfillmentText?.isNotEmpty ?? false) {
      fulfillmentText = fulfillmentText?.replaceAll('\n', '');
      ChatMessage botMessage = ChatMessage(
        text: fulfillmentText!,
        name: "LeadSchool",
        type: false,
      );

      setState(() {
        _messages.insert(0, botMessage);
        _isLoading= false;
      });
    }

    query.add(text);
  }

  void handleStream() async {
    // _recorder.start();

    // _audioStream = BehaviorSubject<List<int>>();
    // _audioStreamSubscription = _recorder.audioStream.listen((data) {
    //   //print(data);
    //   _audioStream?.add(data);
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      ListView.builder(
        padding: const EdgeInsets.only(bottom: 70,left: 10,right: 10,top: 10),
        reverse: true,
        itemBuilder: (_, int index) => _messages[index],
        itemCount: _messages.length,
      ),
      if(_isLoading)const Positioned(
        bottom: 0,
        child: Padding(
          padding: EdgeInsets.only(bottom: 55,left: 10,right: 10,top: 10),
          child: ThreeBounce(size:20,color:Colors.red),
        ),
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: Container(
            decoration: BoxDecoration(color: Theme.of(context).cardColor),
            child: IconTheme(
              data: IconThemeData(color: Theme.of(context).accentColor),
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: _textController,
                        decoration: InputDecoration.collapsed(hintText: "Send a message"),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 4.0),
                      child: IconButton(
                        icon: Icon(Icons.send,color: Colors.blue,),
                        onPressed: () => handleSubmitted(_textController.text),
                      ),
                    ),
                    if (false) IconButton(
                      iconSize: 30.0,
                      icon: Icon(_isRecording ? Icons.mic_off : Icons.mic),
                      onPressed: _isRecording ? stopStream : handleStream,
                    ),
                  ],
                ),
              ),
            )
        ),
      )

    ]);
  }
}


//------------------------------------------------------------------------------------
// The chat message balloon
//
//------------------------------------------------------------------------------------
class ChatMessage extends StatelessWidget {
  ChatMessage({this.text = "", this.name ="", this.type=false});

  final String text;
  final String name;
  final bool type;

  List<Widget> otherMessage(context) {
    return <Widget>[
      Container(
        margin: const EdgeInsets.only(right: 16.0),
        child: const CircleAvatar(
            backgroundColor: Color(0xffff610a),
            child: Text('L',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),)),
      ),
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(this.name,
                style: TextStyle(fontWeight: FontWeight.bold)),
            Container(
              margin: const EdgeInsets.only(top: 5.0),
              child: LinkText(text, onLinkTap: (url)async{
                if(await canLaunch(url)){
                  launch(url);
                }
              },),
            ),
          ],
        ),
      ),
    ];
  }

  List<Widget> myMessage(context) {
    return <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(name, style: Theme.of(context).textTheme.subtitle1),
            Container(
              margin: const EdgeInsets.only(top: 5.0),
              child: Text(text),
            ),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(left: 16.0),
        child: CircleAvatar(
            backgroundColor: Colors.blue,
            child: Text(
              this.name[0],
              style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
            )),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: this.type ? myMessage(context) : otherMessage(context),
      ),
    );
  }
}