// import 'dart:io';
//
// import 'package:flutter/material.dart';
// import 'package:speech_to_text/speech_recognition_result.dart';
// import 'package:speech_to_text/speech_to_text.dart';
//
//
//
// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'TEST',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(),
//     );
//   }
// }
//
// class MyHomePage  extends StatefulWidget{
//   @override
//   State<StatefulWidget> createState() {
//     return MyHomePageState();
//   }
//
// }
//
// class MyHomePageState extends State<MyHomePage>{
//   final _textController = TextEditingController();
//   final SpeechToText _speechToText= SpeechToText();
//   var _isRecording= false;
//   String? _query= null;
//
//   @override
//   void initState() {
//     _init();
//     super.initState();
//   }
//
//   void _init()async{
//     _isRecording = await _speechToText.initialize(debugLogging:true,onStatus: _onStatus,onError: (e){
//       print('errrrrrrr $e');
//     });
//   }
//
//   _onStatus (String text){
//     print('stttttttt $text');
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//           child:Stack(
//             children: [
//               Align(
//                 alignment: Alignment.topCenter,
//                 child: SingleChildScrollView(
//                   child: Image.network('http://api.wolframalpha.com/v1/simple?input=${_query}&appid=demo',
//
//                   ),
//                 ),
//               ),
//               Align(
//                 alignment: Alignment.bottomCenter,
//                 child: Container(
//                     decoration: BoxDecoration(color: Theme.of(context).cardColor),
//                     child: IconTheme(
//                       data: IconThemeData(color: Theme.of(context).accentColor),
//                       child: Container(
//                         margin: const EdgeInsets.symmetric(horizontal: 8.0),
//                         child: Row(
//                           children: <Widget>[
//                             Flexible(
//                               child: TextField(
//                                 controller: _textController,
//                                 onSubmitted: handleSubmitted,
//                                 decoration: InputDecoration.collapsed(hintText: "Send a message"),
//                               ),
//                             ),
//                             Container(
//                               margin: EdgeInsets.symmetric(horizontal: 4.0),
//                               child: IconButton(
//                                 icon: Icon(Icons.send),
//                                 onPressed: () => handleSubmitted(_textController.text),
//                               ),
//                             ),
//                             GestureDetector(
//                               onTapDown: (d){
//                                 if(_speechToText.isNotListening){
//                                   _isRecording = true;
//                                   _speechToText.listen();
//                                 }
//                               },
//                               onTapUp: (d){
//                                 if(_speechToText.isListening){
//                                   _isRecording = false;
//                                   _speechToText.stop();
//                                 }
//                               },
//                               child: Icon(_isRecording ? Icons.mic_off : Icons.mic),
//
//                             ),
//                           ],
//                         ),
//                       ),
//                     )
//                 ),
//               )
//             ],
//           )
//       ),
//     );
//   }
//
//   void onResulr(SpeechRecognitionResult result){
//     _textController.text = result.recognizedWords;
//   }
//
//
//   void handleSubmitted(String value) {
//     setState(() {
//       _query = value;
//     });
//   }
// }
//
//
